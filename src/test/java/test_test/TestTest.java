package test_test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class TestTest {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("http://hflabs.github.io/suggestions-demo/");



        WebElement webElement = driver.findElement(By.id("fullname-surname"));
        webElement.sendKeys("Иванов" + Keys.ENTER);

        webElement = driver.findElement(By.id("fullname-name"));
        webElement.sendKeys("Иван" + Keys.ENTER);

        webElement = driver.findElement(By.id("fullname-patronymic"));
        webElement.sendKeys("Иванович" + Keys.ENTER);



        webElement = driver.findElement(By.id("phone"));
        webElement.sendKeys("+7 916 144-88-99" + Keys.ENTER);

        webElement = driver.findElement(By.id("email"));
        webElement.sendKeys("jaba@mail.ru" + Keys.ENTER);

        webElement = driver.findElement(By.id("address"));
        webElement.sendKeys("Кировская обл, Верхошижемский р-н, деревня Москва, д 4" + Keys.ENTER);

        webElement = driver.findElement(By.id("address-postal_code"));
        webElement.sendKeys("613310" + Keys.ENTER);

        webElement = driver.findElement(By.id("address-region"));
        webElement.sendKeys("обл Кировская" + Keys.ENTER);

        webElement = driver.findElement(By.id("address-city"));
        webElement.sendKeys("р-н Верхошижемский, д Москва" + Keys.ENTER);

        webElement = driver.findElement(By.id("address-house"));
        webElement.sendKeys("д 4" + Keys.ENTER);

        webElement = driver.findElement(By.id("message"));
        webElement.sendKeys("Сообщение сообщение" + Keys.ENTER);

        webElement = driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button"));
        webElement.click();

        Thread.sleep(5000);


                webElement = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/h4"));
                Assert.assertEquals(webElement.getText(), "Это не настоящее правительство :-(");

                webElement = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/p[1]"));
                Assert.assertEquals(webElement.getText(), "К сожалению, мы не можем принять ваше обращение.\n" +
                        "Но вы всегда можете отправить его через электронную приемную правительства Москвы.");

                webElement = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/p[2]/button"));
                Assert.assertEquals(webElement.getText(), "Хорошо, я понял");


    }
}
